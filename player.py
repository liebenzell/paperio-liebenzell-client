import pygame


class Player:
    def __init__(self, player_id, name, direction, position, color):
        self.id = player_id
        self.name = name
        self.direction = direction
        self.pos_x = position[0]
        self.pos_y = position[1]
        self.color = pygame.Color(color)
        self.color_tail = pygame.Color(color).correct_gamma(0.5)
