import json
import pygame
import time
from game import Game
from connection import Connection

HOST = "localhost"
PORT = 1893
WIDTH = 1100
HEIGH = 700
ZOOM = 2


def on_message(client, message):
    """
    Diese Funktion ist eine Callback-Funktion von der WebSocket-Klasse. Sie wird immer aufgerufen, wenn eine
    Nachricht vom Server kommt. Hier müssen also z.B. Events wie 'welcome', 'update', 'game over' oder 'error'
    verarbeitet werden. ACHTUNG: Exceptions werden alle von der verwendeten Bibliothek vollständig abgefangen,
    deshalb wird bei einem Fehler der Code unvollständig oder gar nicht ausgeführt!
    :param client: WebSocket-Clientobjekt
    :param message: Message-Objekt im JSON-Format; muss decoded werden, bei einem Python-Dictionary beispielsweise:
                    dict = json.loads(message)
    :return: Nichts
    """
    data = json.loads(message)
    if data['event'] == 'welcome':
        game.welcome(data['config'], data['player_id'], data['players'], data['ownership'], data['tails'])
    elif data['event'] == 'update':
        game.last_update = time.time() * 1000
        game.update(data['players'], data['kills'], data['clean_owner'], data['clean_tail'], data['new_ownership'])
    elif data['event'] == 'game over':
        game.is_in_game = False
        game.game_over = True
    elif data['event'] == 'error':
        game.error = data['message']


if __name__ == "__main__":
    """
    Hier wird das GUI gemacht.
    In einer Dauerschleife werden die Frames berechnet und Tasteneingaben verarbeitet.
    """

    # Initialisierung
    pygame.init()  # Pygame lädt alle Module
    display = pygame.display.set_mode((WIDTH, HEIGH), pygame.RESIZABLE)  # ein Fenster mit einer Surface wird erstellt
    clock = pygame.time.Clock()  # Pygame Uhr wird geladen
    font = pygame.font.SysFont(pygame.font.get_default_font(), 80)  # Schriftart wird geladen

    # Objekte der Klassen 'Game' und 'Connection' werden erzeugt
    game = Game()
    conn = Connection(HOST, PORT, on_message)  # hier wird die Funktion 'on_message' als Callback-Argument übergeben

    # Farben-Tuples
    WHITE = (255, 255, 255)
    GRAY = (180, 180, 180)

    # Endlosschleife für Darstellung
    while True:

        clock.tick(60)  # Pause, damit es 60 Frames pro Sekunde gibt

        # Aufgetretene Pygame-Events (Liste) werden verarbeitet
        for event in pygame.event.get():

            # wenn Schließen-Button des Fensters angeklickt wurde
            if event.type == pygame.QUIT:
                conn.client.close()  # WebSocket-Verbindung wird vom Client geschlossen
                quit()  # Programm wird beendet

            # wenn es gedrückte Tasten gibt (KEYDOWN)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    # Richtungsevent wird an den Server als JSON gesendet
                    conn.send({'event': "go_direction", 'direction': "right"})
                elif event.key == pygame.K_LEFT:
                    conn.send({'event': "go_direction", 'direction': "left"})
                elif event.key == pygame.K_UP:
                    conn.send({'event': "go_direction", 'direction': "up"})
                elif event.key == pygame.K_DOWN:
                    conn.send({'event': "go_direction", 'direction': "down"})
                elif event.key == pygame.K_RETURN:
                    # Wenn Enter gedrückt wurde und es davor Game Over oder
                    # einen Fehler gab wird die WebSocket-Verbindung neu
                    # aufgebaut und Game Over oder der Fehler zurückgesetzt.
                    if game.game_over:
                        conn = Connection(HOST, PORT, on_message)
                        game.game_over = False
                    elif game.error:
                        conn = Connection(HOST, PORT, on_message)
                        game.error = None

            # wenn die Fenstergröße verändert wurde, wird die Display Surface mit aktueller Größe erstellt
            elif event.type == pygame.VIDEORESIZE:
                display = pygame.display.set_mode((event.w, event.h), pygame.RESIZABLE)
                display.fill(WHITE)  # und weiß gefüllt, damit es keine schwarzen Ränder gibt

        # Eine andere Surface wird erstellt, die um Faktor 'ZOOM' größer ist, als die Display Surface
        world = pygame.Surface((display.get_width() * ZOOM, display.get_height() * ZOOM))
        world.fill(WHITE)  # sie wird weiß gefüllt, damit alte Elemente verschwinden

        if game.is_in_game:
            display.fill(WHITE)
            own_pos_x = None
            own_pos_y = None
            unit = round(min(world.get_height() * 0.9 / game.field_height,
                             world.get_width() * 0.9 / game.field_width))

            for i in range(game.field_width):
                for j in range(game.field_height):
                    players = game.players.copy()
                    border_x = (world.get_width() - unit * game.field_width) / 2
                    border_y = (world.get_height() - unit * game.field_height) / 2
                    size = (int(border_x + i * unit), int(border_y + j * unit), int(unit), int(unit))
                    if game.ownership[i][j] in game.players:
                        color = game.players.get(game.ownership[i][j]).color
                        pygame.draw.rect(world, color, size, 0)
                    else:
                        pygame.draw.rect(world, GRAY, size, 1)
                    if game.tails[i][j] in game.players and \
                            (i, j) != (game.players.get(game.tails[i][j]).pos_x,
                                       game.players.get(game.tails[i][j]).pos_y):
                        pygame.draw.rect(world, players.get(game.tails[i][j]).color_tail, size, 0)

                    players = game.players.copy().values()
                    for p in players:
                        since_last_update = time.time() * 1000 - game.last_update
                        move = unit * since_last_update / (game.time_step * 1000) - unit
                        move = min(0, move)
                        if p.direction == "up":
                            pos_x = border_x + p.pos_x * unit
                            pos_y = (border_y + p.pos_y * unit) - move
                        elif p.direction == "down":
                            pos_x = border_x + p.pos_x * unit
                            pos_y = (border_y + p.pos_y * unit) + move
                        elif p.direction == "left":
                            pos_x = (border_x + p.pos_x * unit) - move
                            pos_y = border_y + p.pos_y * unit
                        else:  # right
                            pos_x = (border_x + p.pos_x * unit) + move
                            pos_y = border_y + p.pos_y * unit

                        pygame.draw.rect(world, WHITE, (int(pos_x), int(pos_y), int(unit), int(unit)), 0)
                        pygame.draw.rect(world, p.color_tail, (int(pos_x), int(pos_y), int(unit), int(unit)), 3)

                        if p.id == game.player_id:
                            own_pos_x = -pos_x + display.get_width() / 2
                            own_pos_y = -pos_y + display.get_height() / 2

            display.blit(world, (int(own_pos_x), int(own_pos_y)))

        elif not game.is_in_game and not game.game_over and not conn.is_open:
            display.fill(WHITE)
            text = font.render('Waiting for connection...', False, (0, 0, 0))
            text_rect = text.get_rect(center=(int(display.get_width() / 2), int(display.get_height() / 2)))
            display.blit(text, text_rect)

        elif not game.is_in_game and game.game_over:
            conn.client.close()

            display.fill(WHITE)
            text = font.render('GAME OVER', False, (0, 0, 0))
            text_rect = text.get_rect(center=(int(display.get_width() / 2), int(display.get_height() / 2)))
            display.blit(text, text_rect)

            text = font.render('Press ENTER to start a new game.', False, (0, 0, 0))
            text_rect = text.get_rect(center=(int(display.get_width() / 2), int(display.get_height() / 4 * 3)))
            display.blit(text, text_rect)

        elif game.error:
            display.fill(WHITE)
            text = font.render(f'error: {game.error}', False, (0, 0, 0))
            text_rect = text.get_rect(center=(int(display.get_width() / 2), int(display.get_height() / 2)))
            display.blit(text, text_rect)

            text = font.render('Press ENTER to try again.', False, (0, 0, 0))
            text_rect = text.get_rect(center=(int(display.get_width() / 2), int(display.get_height() / 4 * 3)))
            display.blit(text, text_rect)

        pygame.display.update()
