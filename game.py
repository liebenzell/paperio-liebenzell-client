from player import Player


class Game:
    def __init__(self):
        self.player_id = None
        self.field_width = None
        self.field_height = None
        self.time_step = None
        self.players = None
        self.ownership = None
        self.tails = None
        self.is_in_game = False
        self.game_over = False
        self.last_update = 0
        self.error = None

    def welcome(self, config, player_id, players, ownership, tails):
        self.player_id = player_id
        self.field_width = config['field_width']
        self.field_height = config['field_height']
        self.time_step = config['time_step']
        self.players = {
            player['id']: Player(player['id'], player['name'], player['direction'], player['position'], player['color'])
            for player in players}
        self.ownership = ownership
        self.tails = tails
        self.is_in_game = True
        self.error = None

    def update(self, players, kills, clean_owner, clean_tail, new_ownership):
        for player in players:
            if player['id'] in self.players:
                self.players[player['id']].direction = player['direction']
                self.players[player['id']].pos_x = player['position'][0]
                self.players[player['id']].pos_y = player['position'][1]
                if player['new_tail']:
                    self.tails[player['position'][0]][player['position'][1]] = player['id']
            else:
                self.players[player['id']] = Player(player['id'], player['name'], player['direction'],
                                                    player['position'], player['color'])
        for kill in kills:
            del self.players[kill]
        for x, y in clean_owner:
            self.ownership[x][y] = 0
        for x, y in clean_tail:
            self.tails[x][y] = 0
        for player_id in new_ownership:
            for x, y in new_ownership[player_id]:
                self.ownership[x][y] = int(player_id)
