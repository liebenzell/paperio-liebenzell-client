import websocket
import threading
import json


class Connection:
    def __init__(self, host, port, on_message):
        self.is_open = False
        self.client = websocket.WebSocketApp(
            f"ws://{host}:{port}",
            on_message=on_message,
            on_error=self.on_error,
            on_open=self.on_open,
            on_close=self.on_close,
        )

        threading.Thread(target=self.client.run_forever).start()

    def on_open(self):
        self.is_open = True
        self.send({'event': 'connect'})

    def on_close(self):
        self.is_open = False

    @staticmethod
    def on_error(error):
        print("Error:", error)

    def send(self, message):
        if not self.is_open:
            return
        try:
            self.client.send(json.dumps(message))
        except websocket.WebSocketConnectionClosedException:
            pass
